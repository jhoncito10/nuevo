import { Router } from '@angular/router';
import { RulesService } from './../services/rules/rules.service';
import { FuncionesService } from './../services/funciones/funciones.service';
import { AnonymousService } from './../services/anonymous/anonymous.service';
import { GoogleService } from './../services/google/google.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {

  my_list : any;
  role = {nombre :null, 
          permisos :{convenios : {lectura:false,escritura:false},
                     roles : {lectura:false, escritura:false},
                    usuarios:{lectura:false, escritura:false}},
          componentes :{ids :null} };

  show_form = false;
  editing = false;
  key = null;

  constructor(public ad: AngularFireDatabase, 
              public auth: AngularFireAuth,
              public goo: GoogleService,
              public anon: AnonymousService,
              public funcion: FuncionesService,
              public rules:RulesService,
              public rout: Router) {

    if((goo.usuario||anon.usuario)){
      this.funcion.suscribirRol().then(()=>{
       this.my_list = this.funcion.getRoles();
       this.rout.navigate(['/home']);
      });        
    }else{
      this.rout.navigate(['/login']);
    }
   }

  ngOnInit() {
  }
  addRol(){
    this.show_form = true;
    this.editing = false;
    this.role = {nombre :null, 
      permisos :{convenios : {lectura:false,escritura:false},
                 roles : {lectura:false, escritura:false},
                usuarios:{lectura:false, escritura:false}},
      componentes :{ids :null} };
  }

  viewRol(object){
    this.editing = true;
    this.role = object;
    this.show_form = true;
    this.key = object.$key;
    console.log(this.role);
  }

  removeRol(){
   this.funcion.eliminaRol(this.key);
   console.log(this.key);
   this.rules.LeerReglas().then(()=>{

    this.rules.eliminaReglas(this.role,this.key).then(()=>{
      this.rules.EnvioReglas().then(()=>{
        console.log('reglas elimiandas');
      });
      
    });

   });
    
  }

  cancelRol(){
    this.show_form = false;
  }

  creaRol(){

    if(this.editing){
      this.funcion.editataRol(this.role,this.key);
    }else{
      this.funcion.creaRol(this.role).then(data=>{
          console.log(data.key);
          console.log(this.role);
          this.rules.preparaRules(this.role, data.key).then(()=>{
            this.rules.EnvioReglas().then(()=>{
              console.log('Reglas publicadas');
            });
            
          });

          this.show_form = false;
          this.role = {nombre :null, 
            permisos :{convenios : {lectura:false,escritura:false},
                       roles : {lectura:false, escritura:false},
                      usuarios:{lectura:false, escritura:false}},
            componentes :{ids :null} };
      });
    }
  }

}
