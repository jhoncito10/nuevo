
//servicios
import { FuncionesService } from './../services/funciones/funciones.service';
import { AnonymousService } from './../services/anonymous/anonymous.service';
import { GoogleService } from './../services/google/google.service';

//rutas
import { Router } from '@angular/router';

import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  dataUser = {
    direccion:"",
    displayname:"", 
    email:null, 
    empresa:"", 
    roles:null,
    telefono:""
  };

  dataAuten = {
    email:null,
    password:null
  };


  constructor(public ad:AngularFireDatabase, 
              public goo:GoogleService, 
              public anon:AnonymousService,
              public funcion:FuncionesService,
              public rout: Router) { 
   
  }

  ngOnInit() {
  }

  insertaUser(provee:string){  
    if(this.funcion.getRoles!=null)
    {   
      this.dataUser.roles = this.funcion.roles[0].$key;

        if(provee == "google")
        {                  
            this.goo.login().then(()=>{
              this.dataUser.displayname = this.goo.usuario.user.displayName;
              this.dataUser.email = this.goo.usuario.user.email; 
              this.ad.database.ref('/usuarios/'+this.goo.usuario.user.uid).set(this.dataUser).then(()=>{
                this.rout.navigate(['/home']);   
              });               
            });         
        }else if(provee == "anonimo"){
          if((this.dataAuten.email!=null)&&(this.dataAuten.password!=null))
          {
            this.anon.createUser(this.dataAuten.email,this.dataAuten.password).then(()=>{
              console.log('usuario creado con exito');
              this.anon.login(this.dataAuten.email,this.dataAuten.password).then(()=>{
                this.ad.database.ref('/usuarios/'+this.anon.usuario.uid).set(this.dataUser).then(()=>{
                  this.rout.navigate(['/home']);   
                }); 
                console.log(this.anon.usuario); 
              });  
                         
            }).catch(()=>{console.log('error al crear usuario');});
          }else{
            console.log('llene por favor los campos pas y email');
          }    
        }
    }else{
      console.log('no se han podido cargar los roles');
      this.rout.navigate(['/registro']);
    }
   }

  

  cancelar(){
    this.rout.navigate(['/login']);
  }

}
