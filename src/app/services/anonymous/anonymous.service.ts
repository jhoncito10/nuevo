import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class AnonymousService {

  usuario:any = {};
  creado:boolean;

  constructor(public afAuth:AngularFireAuth) {
    this.creado = false;
    if(localStorage.getItem('usuario')){
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    }else{
      this.usuario = null;
    }
   }

 login(email:string, pass: string){

    let promise = new Promise((resolve,reject)=>{

      this.afAuth.auth.signInWithEmailAndPassword(email,pass)
      .then(data=>{
        console.log('login');
        this.usuario = data;
        localStorage.setItem('usuario',JSON.stringify(data));
        resolve();
      }); 
    });

    return promise;
 } 

 logout(){
  this.afAuth.auth.signOut();
   localStorage.removeItem('usuario');
   this.usuario = null;
    console.log('usuario deslogueado');
 }

 createUser(email:string, pass:string){
  
      let promise = new Promise((resolve, reject)=>{
  
        this.afAuth.auth.createUserWithEmailAndPassword(email,pass).then(
          () =>{
            console.log('usuario anonimo creado');
            resolve();
        }).catch(function(error){
            console.log(error.message);
        });    
      });
  
     return promise;
 }

  

}
