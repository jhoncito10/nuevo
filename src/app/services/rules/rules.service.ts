import { element } from 'protractor';
import { Injectable } from '@angular/core';

import { Http, Headers } from '@angular/http';

@Injectable()
export class RulesService {

  data:any;

  constructor(public http:Http) {  }


  inform = "";
  url = 'https://univalle-6b018.firebaseio.com/.settings/rules.json?auth=GjMkJg7diYDY9zxo4RTO3R4Lufyd884kBRya90es';
  json:any;
  tablas = [];
 
  LeerReglas(){
    let promise = new Promise((resolve,reject)=>{
      this.data = null;   
          this.http.get(this.url)
          .subscribe(data=>{
            this.data = data.json();
             
            for (var e in this.data.rules) {
              this.tablas.push(e.valueOf());
            }    
            console.log(this.data);
            resolve();
          });
         
          
    });
    return promise;
  }


  preparaRules(role:any,key:string){

    this.inform = " || (root.child('usuarios').child(auth.uid).child('roles').val() == '"+key+"')";

    let promise = new Promise((resolve, reject)=>{
     this.LeerReglas().then(()=>{    

         for (var index = 0; index < this.tablas.length; index++) {

            if(role.permisos[this.tablas[index]].lectura==true){ 
              this.data.rules[this.tablas[index]]['.read'] += this.inform;
              
              // if(this.tablas[index]!="usuarios"){
               
              // }
              // else{
              //   this.data.rules[this.tablas[index]]['$uid']['.read'] +=this.inform;
              // }
            }
            if(role.permisos[this.tablas[index]].escritura==true){
              this.data.rules[this.tablas[index]]['.write'] += this.inform;
              
              // if(this.tablas[index]!="usuarios"){
                
              // }else{
              //   this.data.rules[this.tablas[index]]['roles']['.write'] +=this.inform;
              // }
              
            }
        }
        console.log(this.data);
        resolve();
       });
    });
    return promise;
  }

  EnvioReglas(){
    let promise = new Promise((resolve, reject)=>{
      console.log(this.data);
      this.json = JSON.stringify(this.data);
      let header = new Headers();
      header.append('Content-Type','application/raw');
      this.http.put(this.url,this.json,{
        headers: header
      }).subscribe(data=>{
        console.log(data);
        resolve();
      });
      console.log(this.json);
      resolve();
    });
    return promise;
  }

  auxiliar:string;
  eliminaReglas(role:any,key:string){
    
        let promise = new Promise((resolve, reject)=>{
        this.inform = " || (root.child('usuarios').child(auth.uid).child('roles').val() == '"+key+"')";   

             for (var index = 0; index < this.tablas.length; index++) {
              this.auxiliar = "";
                if(role.permisos[this.tablas[index]].lectura==true){ 
                  if(this.tablas[index]!="usuarios"){
                    this.auxiliar = this.data.rules[this.tablas[index]]['.read'];  
                    
                    this.data.rules[this.tablas[index]]['.read'] = this.auxiliar.replace(this.inform,"");
                   
                  }else{
                    this.auxiliar = this.data.rules[this.tablas[index]]['$uid']['.read'];
                   this.data.rules[this.tablas[index]]['$uid']['.read'] = this.auxiliar.replace(this.inform,"");                   
                  }
                }
                if(role.permisos[this.tablas[index]].escritura==true){
                  if(this.tablas[index]!="usuarios"){
                    this.auxiliar = this.data.rules[this.tablas[index]]['.write'];
                    this.data.rules[this.tablas[index]]['.write'] = this.auxiliar.replace(this.inform,"");
                  }else{
                    this.auxiliar = this.data.rules[this.tablas[index]]['roles']['.write'];
                    this.data.rules[this.tablas[index]]['roles']['.write'] = this.auxiliar.replace(this.inform,"");                   
                  }
                }
            }
            console.log(this.data);
            resolve();
           
     
        });
        return promise;
  }

}
