import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';




@Injectable()
export class GoogleService {

  usuario:any = {};

  constructor(private afAuth: AngularFireAuth) {
    if(localStorage.getItem('usuario')){
      this.usuario = JSON.parse(localStorage.getItem('usuario'));
    }else{
      this.usuario = null;
    }
   }

//    login(): Observable<any>{
   
//       this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
//           .then(data=>{
//             this.usuario = data;
//             localStorage.setItem('usuario',JSON.stringify(data));   
//             console.log('entro');     
//           });  
//           console.log('ya termino');   
//       return this.usuario;
//     } 
// 
   login(){
    let promesa = new Promise((resolve,reject)=>{
      
      this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
          .then(data=>{
            this.usuario = data;
            localStorage.setItem('usuario',JSON.stringify(data));
            resolve();   
          });            
     });
          
      return promesa;
    }

  
   
  logout(){  

     localStorage.removeItem('usuario');
     this.afAuth.auth.signOut();
     this.usuario = null;   
     
  }

}
