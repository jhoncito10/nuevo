import { AngularFireDatabase } from 'angularfire2/database';
import { AnonymousService } from './../anonymous/anonymous.service';
import { GoogleService } from './../google/google.service';
import { Injectable } from '@angular/core';

@Injectable()
export class FuncionesService {


  roles:any;
  usuarios:any;

  constructor(public goo:GoogleService, public anon: AnonymousService,public ad: AngularFireDatabase) {

    this.roles = null;
    this.usuarios = null;
    this.suscribirRol();
    this.suscribirUser();
   }

   suscribirRol(){
    let prom = new Promise((resolve,reject)=>{

      this.roles = null;
      this.getRolesBD().subscribe(
        rol =>{
              this.roles = rol;
              resolve();
        });     
    });  
    return prom;
   }
  getRolesBD(){
   return this.ad.list('/roles');
  }
  getRoles(){
    return this.roles;
  }
  editataRol(role:any, key:string){
    this.ad.database.ref('/roles/'+key).set(role);
  }
  creaRol(role:any){
    return this.ad.database.ref('/roles').push(role);
  }
  eliminaRol(key:string){  
    this.ad.database.ref('roles/'+key).remove();
    
  }

  suscribirUser(){
    let prom = new Promise((resolve,reject)=>{

      this.usuarios = null;
      this.getUsersBD().subscribe(
        user =>{
              this.usuarios = user;
              resolve();
        });     
    });  
    return prom;
   }

  getUsersBD(){
   return this.ad.list('/usuarios');
  }
  getUser(){
    return this.usuarios;
  }
  editarUser(user:any, key:string){
    this.ad.database.ref('/usuarios/'+key).set(user);
  }
  creaUser(user:any){
    return this.ad.database.ref('/usuarios').push(user);
  }
  eliminaUser(key:string){  
    this.ad.database.ref('usuarios/'+key).remove();
  }

}
