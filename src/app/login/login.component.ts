

import { FuncionesService } from './../services/funciones/funciones.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { AnonymousService } from './../services/anonymous/anonymous.service';

import { Router } from '@angular/router';

import { GoogleService } from './../services/google/google.service';

import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent{

  email:string;
  pass:string;
  
  constructor(public ad: AngularFireDatabase,
              public goo:GoogleService, 
              public anon:AnonymousService,
              public funcion:FuncionesService,
              public rout: Router) { 
    
  }

  login(provee: string){
    console.log('entro login');
      if(provee == "google")
      {
        this.goo.login().then(()=>{
         this.rout.navigate(['/home']);
        }).catch(()=>{
          console.log('error');
        });
       
      }else{
        this.anon.login(this.email, this.pass).then(()=>{
          this.rout.navigate(['/home']);
        }); 
      }   
  }

  logout(){
    this.goo.logout();
    this.anon.logout();
    console.log('deslogueado');
    this.rout.navigate(['/login']);
  }


  registro(){
    this.rout.navigate(['/registro']);
  }


 


}
