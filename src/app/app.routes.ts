
//componentes
import { RegistroComponent } from './registro/registro.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';


import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const app_routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'registro', component: RegistroComponent},
    {path: 'home', component: HomeComponent},
    {path: '', pathMatch: 'full', redirectTo: 'login'}
];

export const app_routing = RouterModule.forRoot(app_routes, {useHash:true});
