import { Router } from '@angular/router';
import { RulesService } from './../services/rules/rules.service';
import { FuncionesService } from './../services/funciones/funciones.service';
import { AnonymousService } from './../services/anonymous/anonymous.service';
import { GoogleService } from './../services/google/google.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  my_list : any;
  user = {  direccion:null,
            displayname:null,
            email:null,
            empresa:null,
            roles:null,
            telefono:null
          };
  
  roles : any;

  show_form = false;
  editing = false;
  key = null;

  constructor(public ad: AngularFireDatabase, 
              public auth: AngularFireAuth,
              public goo: GoogleService,
              public anon: AnonymousService,
              public funcion: FuncionesService,
              public rules:RulesService,
              public rout: Router) {
                if((goo.usuario||anon.usuario)){

                  console.log(goo.usuario);
                  this.funcion.suscribirUser().then(()=>{
                   this.my_list = this.funcion.getUser();                  
                  });
                  this.funcion.suscribirRol().then(()=>{
                    this.roles = this.funcion.getRoles();
                    console.log(this.roles);
                    this.rout.navigate(['/home']);
                   });
                          
                }else{
                  this.rout.navigate(['/login']);
                }
               }

  ngOnInit() {
  }

  addUser(){
    this.show_form = true;
    this.editing = false;
    this.user = {  direccion:null,
                  displayname:null,
                  email:null,
                  empresa:null,
                  roles:null,
                  telefono:null
    };
  }

  viewUser(object){
    this.editing = true;
    this.user = object;
    this.show_form = true;
    this.key = object.$key;
    console.log(this.user);
  }

  removeUser(){
   this.funcion.eliminaUser(this.key);
   console.log(this.key);
  }

  cancelUser(){
    this.show_form = false;
  }

  creaUser(){

    if(this.editing){
      this.funcion.editarUser(this.user,this.key);
      this.show_form = false;
      this.rout.navigate['/home'];
    }else {
      this.funcion.creaUser(this.user).then(data=>{
          console.log(data.key);
          console.log(this.user);
          });

          this.show_form = false;
          this.user = {  direccion:null,
                        displayname:null,
                        email:null,
                        empresa:null,
                        roles:null,
                        telefono:null
          };
    
    }
  }

}
