

//rutas
import { app_routing } from './app.routes';
//servicios de login
import { AnonymousService } from './services/anonymous/anonymous.service';
import { GoogleService } from './services/google/google.service';
import { FuncionesService } from './services/funciones/funciones.service';
import { RulesService } from './services/rules/rules.service';

//credenciales BD
import { environment } from './../environments/environment';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

//componentes
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { RegistroComponent } from './registro/registro.component';

import { HttpModule } from '@angular/http';
import { RolesComponent } from './roles/roles.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegistroComponent,
    RolesComponent,
    UsuariosComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    app_routing,
    HttpModule
    
  ],
  providers: [GoogleService,AnonymousService, FuncionesService, RulesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
