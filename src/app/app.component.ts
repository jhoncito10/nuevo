import { AnonymousService } from './services/anonymous/anonymous.service';
import { GoogleService } from './services/google/google.service';
import { Component } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 constructor(public goo:GoogleService, public anon: AnonymousService){
  
 }

}
