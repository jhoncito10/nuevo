import { RulesService } from './../services/rules/rules.service';
import { FuncionesService } from './../services/funciones/funciones.service';
import { Router } from '@angular/router';

import { AnonymousService } from './../services/anonymous/anonymous.service';
import { GoogleService } from './../services/google/google.service';



import { Component } from '@angular/core';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent{
  title = 'app';
  
    constructor(public goo: GoogleService,
                public anon: AnonymousService,
                public rout: Router){

     if(!(goo.usuario||anon.usuario)){
      this.rout.navigate(['/login']);
     }
    }  

}
