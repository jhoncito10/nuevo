// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  
  firebase : {
    apiKey: "AIzaSyDPia8Gzpu5raWQHqT0FBtdrBcMkjo0Ndc",
    authDomain: "univalle-6b018.firebaseapp.com",
    databaseURL: "https://univalle-6b018.firebaseio.com",
    projectId: "univalle-6b018",
    storageBucket: "univalle-6b018.appspot.com",
    messagingSenderId: "255416933465"
  }
};
